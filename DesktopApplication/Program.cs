using System;
using System.Windows.Forms;
using Tetris.Rule;

namespace Tetris.DesktopApp
{
	static class Program
	{
		/// <summary>
		///  The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.SetHighDpiMode(HighDpiMode.SystemAware);
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			var field = new SimpleField();
			var rule = new SimpleRule();
			var game = new Game(rule, field);
			Application.Run(new MainForm(game, field));
		}
	}
}
