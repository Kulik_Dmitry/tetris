﻿using System.Windows.Forms;
using Tetris.Rule;

namespace Tetris.DesktopApp
{
	public partial class MainForm : Form
	{
		private PlayingFieldControl playingFieldControl;
		private readonly IGame game;

		public MainForm(IGame game, IField field)
		{
			InitializeComponent();
			this.game = game;
			var fieldStyle = new PlainFieldStyle();
			playingFieldControl = new PlayingFieldControl(fieldStyle, field)
			{
				Dock = DockStyle.Fill
			};
			panelField.Controls.Add(playingFieldControl);
			Load += MainForm_Load;
		}

		private void MainForm_KeyDown(object sender, KeyEventArgs e)
		{
			if (e.KeyData == Keys.A)
			{
				game.MoveFigureLeft();
			}
			if (e.KeyData == Keys.D)
			{
				game.MoveFigureRight();
			}
			if (e.KeyData == Keys.S && !accelerationOn)
			{
				accelerationOn = true;
				game.Acceleration();
			}
			if (e.KeyData == Keys.Space)
			{
				game.RotateFigure();
			}
		}

		bool accelerationOn;

		private void MainForm_Load(object sender, System.EventArgs e)
		{
			game.Start();
			game.RedrawField += Game_RedrawField;
			KeyDown += MainForm_KeyDown;
			KeyUp += MainForm_KeyUp;
		}

		private void MainForm_KeyUp(object sender, KeyEventArgs e)
		{
			if(e.KeyData == Keys.S && accelerationOn)
			{
				accelerationOn = false;
				game.StopAcceleration();
			}
		}

		private void Game_RedrawField()
		{
			playingFieldControl.Invalidate();
		}
	}
}
