﻿
namespace Tetris.DesktopApp
{
	partial class MainForm
	{
		/// <summary>
		///  Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		///  Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		///  Required method for Designer support - do not modify
		///  the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.gameDropButoon = new System.Windows.Forms.ToolStripDropDownButton();
			this.setupDropButoon = new System.Windows.Forms.ToolStripDropDownButton();
			this.panelField = new System.Windows.Forms.Panel();
			this.toolStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// toolStrip1
			// 
			this.toolStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.gameDropButoon,
            this.setupDropButoon});
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(601, 29);
			this.toolStrip1.TabIndex = 0;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// gameDropButoon
			// 
			this.gameDropButoon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.gameDropButoon.Image = global::Tetris.DesktopApp.Properties.Resources.tetris_game_icon_32;
			this.gameDropButoon.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.gameDropButoon.Name = "gameDropButoon";
			this.gameDropButoon.Size = new System.Drawing.Size(38, 24);
			this.gameDropButoon.Text = "Игра";
			// 
			// setupDropButoon
			// 
			this.setupDropButoon.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
			this.setupDropButoon.Image = global::Tetris.DesktopApp.Properties.Resources.setup_32;
			this.setupDropButoon.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.setupDropButoon.Name = "setupDropButoon";
			this.setupDropButoon.Size = new System.Drawing.Size(38, 24);
			this.setupDropButoon.Text = "Настройки";
			// 
			// panelField
			// 
			this.panelField.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelField.Location = new System.Drawing.Point(0, 29);
			this.panelField.Margin = new System.Windows.Forms.Padding(4);
			this.panelField.Name = "panelField";
			this.panelField.Size = new System.Drawing.Size(601, 801);
			this.panelField.TabIndex = 1;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(10F, 25F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(601, 830);
			this.Controls.Add(this.panelField);
			this.Controls.Add(this.toolStrip1);
			this.DoubleBuffered = true;
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.KeyPreview = true;
			this.Margin = new System.Windows.Forms.Padding(4);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "MainForm";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Тетрис";
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.ToolStripDropDownButton gameDropButoon;
		private System.Windows.Forms.ToolStripDropDownButton setupDropButoon;
		private System.Windows.Forms.Panel panelField;
	}
}

