﻿using System.Drawing;
using System.Windows.Forms;
using Tetris.Rule;

namespace Tetris.DesktopApp
{
	public partial class PlayingFieldControl : UserControl
	{
		private readonly IFieldStyle fieldStyle;
		private readonly IField field;
		private readonly int borderThickness = 1;

		public PlayingFieldControl(IFieldStyle fieldStyle, IField field)
		{
			InitializeComponent();
			this.fieldStyle = fieldStyle;
			this.field = field;
		}

		private void PlayingFieldControl_Paint(object sender, PaintEventArgs e)
		{
			BackColor = fieldStyle.BackGroundColor;
			Pen pen = new Pen(fieldStyle.LineColor);
			for (int i = 0; i <= field.WidthCellCount; i++)
			{
				var startPoint = new Point(i * field.CellSize, 0);
				var finishPoint = new Point(i * field.CellSize, Height);
				e.Graphics.DrawLine(pen, startPoint, finishPoint);
			}
			for (int i = 0; i <= field.HeightCellCount; i++)
			{
				var startPoint = new Point(0, i * field.CellSize);
				var finishPoint = new Point(Width, i * field.CellSize);
				e.Graphics.DrawLine(pen, startPoint, finishPoint);
			}
			DrawFilledSpace(e.Graphics);
			DrawFigure(e.Graphics);
		}

		private void DrawFilledSpace(Graphics graphics)
		{
			foreach (var square in field.FilledSquares)
			{
				graphics.FillRectangle(
					fieldStyle.FilledSpaceColor,
					square.LeftTop.X + borderThickness,
					square.LeftTop.Y + borderThickness,
					field.CellSize - borderThickness,
					field.CellSize - borderThickness);
			}
		}

		private void DrawFigure(Graphics graphics)
		{
			foreach (var point in field.CurrentFigure.Squares)
			{
				graphics.FillRectangle(
					fieldStyle.FigureColor,
					point.LeftTop.X + borderThickness,
					point.LeftTop.Y + borderThickness,
					field.CellSize - borderThickness,
					field.CellSize - borderThickness);
			}
		}
	}
}
