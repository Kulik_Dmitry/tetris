﻿using System.Drawing;

namespace Tetris.DesktopApp
{
	public class PlainFieldStyle : IFieldStyle
	{
		public Color LineColor { get; }
		public Color BackGroundColor { get; }
		public Brush FigureColor { get; }
		public Brush FilledSpaceColor { get; }

		public PlainFieldStyle()
		{
			LineColor = Color.Black;
			BackGroundColor = Color.LightGray;
			FigureColor = Brushes.DarkGray;
			FilledSpaceColor = Brushes.DimGray;
		}
	}
}
