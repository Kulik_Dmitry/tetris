﻿using System.Drawing;

namespace Tetris.DesktopApp
{
	public interface IFieldStyle
	{
		Color LineColor { get; }

		Color BackGroundColor { get; }

		Brush FigureColor { get; }

		Brush FilledSpaceColor { get; }
	}
}
