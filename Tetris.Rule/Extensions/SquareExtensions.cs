﻿using System.Collections.Generic;
using System.Linq;

namespace Tetris.Rule
{
	public static class SquareExtensions
	{
		public static Square GetMaxBottomSquare(this IEnumerable<Square> squares)
		{
			var maxBottomY = squares.Max(s => s.LeftBottom.Y);
			return squares.Single(s => s.LeftBottom.Y == maxBottomY);
		}

		public static Square GetMaxLeftSquare(this IEnumerable<Square> squares)
		{
			var maxLeftX = squares.Min(s => s.LeftTop.X);
			return squares.Single(s => s.LeftTop.X == maxLeftX);
		}

		public static Square GetMaxRightSquare(this IEnumerable<Square> squares)
		{
			var maxRightX = squares.Max(s => s.RightTop.X);
			return squares.Single(s => s.RightTop.X == maxRightX);
		}
	}
}
