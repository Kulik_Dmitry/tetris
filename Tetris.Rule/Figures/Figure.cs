﻿namespace Tetris.Rule
{
	public abstract class Figure
	{
		public Square CenterSquare { get; protected set; }

		public Square[] Squares { get; protected set; }

		public Figure(Square square)
		{
			CenterSquare = square;
		}

		public void Initialize()
		{
			Squares = GetSquares();
		}

		public void MoveDown()
		{
			CenterSquare = new Square(CenterSquare.LeftBottom, CenterSquare.CellSize);
			Squares = GetSquares();
		}

		public void MoveLeft()
		{
			var newPoint = CenterSquare.LeftTop;
			newPoint.Offset(-CenterSquare.CellSize, 0);
			CenterSquare = new Square(newPoint, CenterSquare.CellSize);
			Squares = GetSquares();
		}

		public void MoveRight()
		{
			var newPoint = CenterSquare.LeftTop;
			newPoint.Offset(CenterSquare.CellSize, 0);
			CenterSquare = new Square(newPoint, CenterSquare.CellSize);
			Squares = GetSquares();
		}

		protected abstract Square[] GetSquares();
	}
}
