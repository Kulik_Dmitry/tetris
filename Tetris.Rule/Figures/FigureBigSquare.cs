﻿using System.Drawing;

namespace Tetris.Rule
{
	public class FigureBigSquare : Figure
	{
		public FigureBigSquare(Square square) : base(square) { }

		protected override Square[] GetSquares()
		{
			return new[]
			{
				CenterSquare,
				new Square(new Point(CenterSquare.LeftTop.X - CenterSquare.CellSize, CenterSquare.LeftTop.Y), CenterSquare.CellSize),
				new Square(new Point(CenterSquare.LeftTop.X - CenterSquare.CellSize, CenterSquare.LeftTop.Y + CenterSquare.CellSize), CenterSquare.CellSize),
				new Square(new Point(CenterSquare.LeftBottom.X, CenterSquare.LeftBottom.Y), CenterSquare.CellSize)
			};
		}
	}
}
