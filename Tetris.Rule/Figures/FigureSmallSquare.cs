﻿namespace Tetris.Rule
{
	public class FigureSmallSquare : Figure
	{
		public FigureSmallSquare(Square square) : base(square) { }

		protected override Square[] GetSquares()
		{
			return new[] { CenterSquare };
		}
	}
}
