﻿using System.Drawing;

namespace Tetris.Rule
{
	public class FigureLine : RotateableFigure
	{
		public FigureLine(Square square, bool isVertical) : base(square, isVertical) { }
		
		protected override Square[] GetHorizontal()
		{
			return new[]
			{
				CenterSquare,
				new Square(new Point(CenterSquare.LeftTop.X + CenterSquare.CellSize, CenterSquare.LeftTop.Y), CenterSquare.CellSize),
				new Square(new Point(CenterSquare.LeftTop.X - CenterSquare.CellSize, CenterSquare.LeftTop.Y), CenterSquare.CellSize),
				new Square(new Point(CenterSquare.LeftTop.X - 2 * CenterSquare.CellSize, CenterSquare.LeftTop.Y), CenterSquare.CellSize)
			};
		}

		protected override Square[] GetVertical()
		{
			return new[]
						{
				CenterSquare,
				new Square(new Point(CenterSquare.LeftTop.X, CenterSquare.LeftTop.Y - CenterSquare.CellSize), CenterSquare.CellSize),
				new Square(new Point(CenterSquare.LeftTop.X, CenterSquare.LeftTop.Y + CenterSquare.CellSize), CenterSquare.CellSize),
				new Square(new Point(CenterSquare.LeftTop.X, CenterSquare.LeftTop.Y + 2 * CenterSquare.CellSize), CenterSquare.CellSize)
			};
		}
	}
}
