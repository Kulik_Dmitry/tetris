﻿using System.Drawing;

namespace Tetris.Rule
{
	public class FigureCurved : RotateableFigure
	{
		public FigureCurved(Square square, bool isVertical) : base(square, isVertical) { }

		protected override Square[] GetHorizontal()
		{
			return new[]
			{
				CenterSquare,
				new Square(new Point(CenterSquare.LeftTop.X - CenterSquare.CellSize, CenterSquare.LeftTop.Y), CenterSquare.CellSize),
				new Square(new Point(CenterSquare.LeftBottom.X, CenterSquare.LeftBottom.Y), CenterSquare.CellSize),
				new Square(new Point(CenterSquare.RightBottom.X, CenterSquare.RightBottom.Y), CenterSquare.CellSize)
			};
		}

		protected override Square[] GetVertical()
		{
			return new[]
			{
				CenterSquare,
				new Square(new Point(CenterSquare.LeftTop.X, CenterSquare.LeftTop.Y - CenterSquare.CellSize), CenterSquare.CellSize),
				new Square(new Point(CenterSquare.LeftTop.X - CenterSquare.CellSize, CenterSquare.LeftTop.Y), CenterSquare.CellSize),
				new Square(new Point(CenterSquare.LeftBottom.X - CenterSquare.CellSize, CenterSquare.LeftBottom.Y), CenterSquare.CellSize)
			};
		}
	}
}
