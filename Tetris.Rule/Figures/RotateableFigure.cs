﻿namespace Tetris.Rule
{
	public abstract class RotateableFigure : Figure
	{
		private bool isVertical;

		public RotateableFigure(Square square, bool isVertical) : base(square)
		{
			this.isVertical = isVertical;
		}

		public void Rotate()
		{
			isVertical = !isVertical;
			Squares = isVertical ? GetVertical() : GetHorizontal();
		}

		protected override Square[] GetSquares() =>
			isVertical ? GetVertical() : GetHorizontal();

		protected abstract Square[] GetHorizontal();

		protected abstract Square[] GetVertical();
	}
}
