﻿namespace Tetris.Rule
{
	public interface ITetrisRule
	{
		int Interval { get; }

		Figure GetNewFigure(IField field);
	}
}
