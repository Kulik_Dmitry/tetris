﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace Tetris.Rule
{
	public class SimpleRule : ITetrisRule
	{
		private readonly Random random;
		public int Interval { get; }

		private Func<Square, Figure>[] figures = new Func<Square, Figure>[]
		{
			square => new FigureSmallSquare(square),
			square =>
			{
				var isVertical = new Random().Next(0, 2) == 0;
				return new FigureCurved(square, isVertical);
			},
			square => new FigureBigSquare(square),
			square =>
			{
				var isVertical = new Random().Next(0, 2) == 0;
				return new FigureReverseCurved(square, isVertical);
			},
			square =>
			{
				var isVertical = new Random().Next(0, 2) == 0;
				return new FigureLine(square, isVertical);
			},
		};

		public SimpleRule()
		{
			Interval = 200;
			random = new Random();
		}

		public Figure GetNewFigure(IField field)
		{
			var x = (field.WidthCellCount / 2) * field.CellSize;
			var square = new Square(new Point(x, 0), field.CellSize);
			var number = random.Next(0, figures.Length);
			var figure = figures[number].Invoke(square);
			figure.Initialize();
			return figure;
		}
	}
}
