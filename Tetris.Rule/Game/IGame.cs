﻿using System;

namespace Tetris.Rule
{
	public interface IGame
	{
		event Action RedrawField;
		void Start();
		void MoveFigureLeft();
		void MoveFigureRight();
		void Acceleration();
		void StopAcceleration();
		void RotateFigure();
	}
}
