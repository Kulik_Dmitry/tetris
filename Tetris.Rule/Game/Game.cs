﻿using System;
using System.Timers;

namespace Tetris.Rule
{
	public class Game : IGame
	{
		private readonly Timer timer = new Timer();
		private readonly ITetrisRule rule;
		private readonly IField field;

		public event Action RedrawField;

		public Game(ITetrisRule tetrisRule, IField field)
		{ 
			timer.Interval = tetrisRule.Interval;
			rule = tetrisRule;
			this.field = field;
			field.CreateNewFigure(tetrisRule);
			timer.Elapsed += Timer_Elapsed;
		}

		private void Timer_Elapsed(object sender, ElapsedEventArgs e)
		{
			if (!field.FigureCanMoveDown())
			{
				field.FillSpaceSquares();
				field.RemoveFilledLines();
				field.CreateNewFigure(rule);
				RedrawField.Invoke();
			}
			else
			{
				field.CurrentFigure.MoveDown();
				RedrawField.Invoke();
			}
		}

		public void Start()
		{
			timer.Start();
		}

		public void MoveFigureLeft()
		{
			if (field.FigureCanMoveLeft())
			{
				field.CurrentFigure.MoveLeft();
				RedrawField.Invoke();
			}
		}

		public void MoveFigureRight()
		{
			if (field.FigureCanMoveRight())
			{
				field.CurrentFigure.MoveRight();
				RedrawField.Invoke();
			}
		}

		public void Acceleration()
		{
			timer.Interval = rule.Interval / 3;
		}

		public void StopAcceleration()
		{
			timer.Interval = rule.Interval;
		}

		public void RotateFigure()
		{
			if (field.FigureCanMoveDown())
			{
				field.RotateFigure();
				RedrawField.Invoke();
			}
		}
	}
}
