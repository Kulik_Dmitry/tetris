﻿using System.Collections.Generic;

namespace Tetris.Rule
{
	public interface IField
	{
		int CellSize { get; }
		int WidthCellCount { get; }
		int HeightCellCount { get; }
		Figure CurrentFigure { get; }
		List<Square> FilledSquares { get; }

		bool FigureCanMoveDown();
		bool FigureCanMoveLeft();
		bool FigureCanMoveRight();
		void FillSpaceSquares();
		void CreateNewFigure(ITetrisRule tetrisRule);
		void RotateFigure();
		void RemoveFilledLines();
	}
}
