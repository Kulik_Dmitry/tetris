﻿using System.Collections.Generic;
using System.Linq;

namespace Tetris.Rule
{
	public class SimpleField : IField
	{
		public int CellSize { get; private set; }

		public int WidthCellCount { get; private set; }

		public int HeightCellCount { get; private set; }

		public Figure CurrentFigure { get; private set; }

		public List<Square> FilledSquares { get; private set; }

		public SimpleField()
		{
			CellSize = 40;
			WidthCellCount = 15;
			HeightCellCount = 20;
			FilledSquares = new List<Square>();			
		}

		public void FillSpaceSquares()
		{
			FilledSquares.AddRange(CurrentFigure.Squares);
		}

		public void CreateNewFigure(ITetrisRule tetrisRule)
		{
			CurrentFigure = tetrisRule.GetNewFigure(this);
		}

		public bool FigureCanMoveDown()
		{
			var bottomFigureSquares =
				CurrentFigure.Squares.GroupBy(square => square.LeftBottom.X).Select(group => group.GetMaxBottomSquare());

			foreach (var square in bottomFigureSquares)
			{
				var filledSquaresAtSameLevel =
						FilledSquares.Where(fs => fs.LeftBottom.X == square.LeftBottom.X).ToList();
				if (square.LeftBottom.Y == HeightCellCount * CellSize ||
					(filledSquaresAtSameLevel.Count != 0 && filledSquaresAtSameLevel.Any(fs => fs.LeftTop.Y == square.LeftBottom.Y)))
					return false;
			}
			return true;
		}

		public bool FigureCanMoveLeft()
		{
			var leftFigureSquares =
				CurrentFigure.Squares.GroupBy(square => square.LeftTop.Y).Select(group => group.GetMaxLeftSquare());

			foreach(var square in leftFigureSquares)
			{
				var filledSquaresAtSameLevel =
						FilledSquares.Where(fs => fs.LeftTop.Y == square.LeftTop.Y).ToList();
				if (square.LeftTop.X == 0 || 
					(filledSquaresAtSameLevel.Count != 0 && filledSquaresAtSameLevel.Any(fs => fs.RightTop.X == square.LeftTop.X)))
					return false;
			}
			return true;
		}

		public bool FigureCanMoveRight()
		{
			var rightFigureSquares =
				CurrentFigure.Squares.GroupBy(square => square.RightTop.Y).Select(group => group.GetMaxRightSquare());

			foreach (var square in rightFigureSquares)
			{
				var filledSquaresAtSameLevel =
						FilledSquares.Where(fs => fs.RightTop.Y == square.RightTop.Y).ToList();
				if (square.RightTop.X == CellSize * WidthCellCount || 
					(filledSquaresAtSameLevel.Count != 0 && filledSquaresAtSameLevel.Any(fs => fs.LeftTop.X == square.RightTop.X)))
					return false;
			}
			return true;
		}

		public void RotateFigure()
		{
			if (CurrentFigure is RotateableFigure rotateableFigure)
			{
				rotateableFigure.Rotate();
			}
		}

		public void RemoveFilledLines()
		{
			var rows = FilledSquares
				.GroupBy(sq => sq.LeftTop.Y)
				.OrderByDescending(group => group.Key)
				.ToList();

			FilledSquares.Clear();

			short countRemovedLines = 0;
			foreach(var row in rows)
			{
				if (row.Count() == WidthCellCount)
				{
					countRemovedLines++;
				}
				else
				{
					FilledSquares.AddRange(row.Select(sq =>
					{
						var newPoint = sq.LeftTop;
						newPoint.Offset(0, CellSize * countRemovedLines);
						return new Square(newPoint, CellSize);
					}));
				}
			}
		}
	}
}
