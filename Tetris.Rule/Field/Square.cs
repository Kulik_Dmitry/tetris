﻿using System.Drawing;

namespace Tetris.Rule
{
	public struct Square
	{
		public int CellSize { get; }

		public Point LeftTop { get; }
		public Point RightTop { get; }
		public Point LeftBottom { get; }
		public Point RightBottom { get; }

		public Square(Point leftTopPoint, int cellSize)
		{
			LeftTop = leftTopPoint;
			CellSize = cellSize;
			RightTop = new Point(leftTopPoint.X + cellSize, leftTopPoint.Y);
			LeftBottom = new Point(leftTopPoint.X, leftTopPoint.Y + cellSize);
			RightBottom = new Point(leftTopPoint.X + cellSize, leftTopPoint.Y + cellSize);
		}

		public Square(int columnNumber, int rowNumber, int cellSize)
		{
			LeftTop = new Point(columnNumber * cellSize, rowNumber * cellSize);
			CellSize = cellSize;
			RightTop = new Point(LeftTop.X + cellSize, LeftTop.Y);
			LeftBottom = new Point(LeftTop.X, LeftTop.Y + cellSize);
			RightBottom = new Point(LeftTop.X + cellSize, LeftTop.Y + cellSize);
		}

		public override bool Equals(object obj)
		{
			if (obj is Square square)
				return square.LeftTop == LeftTop && square.LeftBottom == LeftBottom &&
					square.RightBottom == RightBottom && square.RightTop == RightTop;

			return false;
		}

		public override int GetHashCode()
		{
			int hashcode = LeftTop.GetHashCode();
			hashcode = 31 * hashcode + RightTop.GetHashCode();
			hashcode = 31 * hashcode + LeftBottom.GetHashCode();
			hashcode = 31 * hashcode + RightBottom.GetHashCode();
			return hashcode;
		}
	}
}
