using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq;

namespace Tetris.Rule.UnitTests
{
	[TestClass]
	public class RemoveFilledLinesTests
	{
		private readonly Mock<ITetrisRule> ruleMock = new Mock<ITetrisRule>();

		[TestMethod]
		public void RemoveLines_FieldHasFilledLine_ShouldRemoveLine()
		{
			var field = new SimpleField();
			FillLine(field, field.HeightCellCount - 1);

			field.RemoveFilledLines();

			Assert.IsTrue(field.FilledSquares.Count == 0);
		}

		[TestMethod]
		public void RemoveLines_FieldHasFilledLineAndOneUpperSquare_ShouldRemoveLine()
		{
			var field = new SimpleField();
			FillLine(field, field.HeightCellCount - 1);
			AddFilledSquare(field, 0, field.HeightCellCount - 2);

			field.RemoveFilledLines();

			var expectedSquare = new Square(0, field.HeightCellCount - 1, field.CellSize);
			var square = field.FilledSquares.Single();
			Assert.AreEqual(expectedSquare, square);
		}

		[TestMethod]
		public void RemoveLines_FieldHasFilledLineAndOneUnderSquare_ShouldRemoveLine()
		{
			var field = new SimpleField();
			FillLine(field, field.HeightCellCount - 2);
			AddFilledSquare(field, 0, field.HeightCellCount - 1);

			field.RemoveFilledLines();

			var expectedSquare = new Square(0, field.HeightCellCount - 1, field.CellSize);
			var square = field.FilledSquares.Single();
			Assert.AreEqual(expectedSquare, square);
		}

		[TestMethod]
		public void RemoveLines_FieldHasFilledLinesAndOneUpperSquare_ShouldRemoveLine()
		{
			var field = new SimpleField();
			FillLine(field, field.HeightCellCount - 1);
			FillLine(field, field.HeightCellCount - 2);
			AddFilledSquare(field, 0, field.HeightCellCount - 3);

			field.RemoveFilledLines();

			var expectedSquare = new Square(0, field.HeightCellCount - 1, field.CellSize);
			var square = field.FilledSquares.Single();
			Assert.AreEqual(expectedSquare, square);
		}

		[TestMethod]
		public void RemoveLines_FieldHasFilledLinesAndUpperSquares_ShouldRemoveLines()
		{
			var field = new SimpleField();
			FillLine(field, field.HeightCellCount - 1);
			FillLine(field, field.HeightCellCount - 3);
			FillLine(field, field.HeightCellCount - 5);
			AddFilledSquare(field, 0, field.HeightCellCount - 2);
			AddFilledSquare(field, 1, field.HeightCellCount - 4);
			AddFilledSquare(field, 2, field.HeightCellCount - 6);

			field.RemoveFilledLines();

			Assert.IsTrue(field.FilledSquares.Count == 3);
			CollectionAssert.AreEquivalent(
				new[] 
				{
					new Square(0, field.HeightCellCount - 1, field.CellSize),
					new Square(1, field.HeightCellCount - 2, field.CellSize),
					new Square(2, field.HeightCellCount - 3, field.CellSize)
				}, field.FilledSquares);
		}

		[TestMethod]
		public void RemoveLine_FieldHasFilledLineAndTwoUpperSquares_ShouldRemoveLine()
		{
			var field = new SimpleField();
			FillLine(field, field.HeightCellCount - 1);
			AddFilledSquare(field, 0, field.HeightCellCount - 2);
			AddFilledSquare(field, 1, field.HeightCellCount - 3);

			field.RemoveFilledLines();

			Assert.IsTrue(field.FilledSquares.Count == 2);
			CollectionAssert.AreEquivalent(
				new[] 
				{
					new Square(0, field.HeightCellCount - 1, field.CellSize),
					new Square(1, field.HeightCellCount - 2, field.CellSize)
				}, field.FilledSquares);
		}

		[TestMethod]
		public void RemoveLines_FieldHasFilledLinesAndOneSquareAboveOther_ShouldRemoveLines()
		{
			var field = new SimpleField();
			FillLine(field, field.HeightCellCount - 1);
			FillLine(field, field.HeightCellCount - 3);
			FillLine(field, field.HeightCellCount - 5);
			AddFilledSquare(field, 0, field.HeightCellCount - 2);
			AddFilledSquare(field, 0, field.HeightCellCount - 4);
			AddFilledSquare(field, 0, field.HeightCellCount - 6);

			field.RemoveFilledLines();

			Assert.IsTrue(field.FilledSquares.Count == 3);
			CollectionAssert.AreEquivalent(
				new[] 
				{
					new Square(0, field.HeightCellCount - 1, field.CellSize),
					new Square(0, field.HeightCellCount - 2, field.CellSize),
					new Square(0, field.HeightCellCount - 3, field.CellSize)
				}, field.FilledSquares);
		}

		private void AddFilledSquare(IField field, int columnNumber, int rowNumber)
		{
			var square = new Square(columnNumber, rowNumber, field.CellSize);
			var figure = new FigureSmallSquare(square);
			figure.Initialize();
			ruleMock.Setup(rule => rule.GetNewFigure(field)).Returns(figure);
			field.CreateNewFigure(ruleMock.Object);
			field.FillSpaceSquares();
		}

		private void FillLine(IField field, int rowNumber)
		{			
			for (int i = 0; i < field.WidthCellCount; i++)
			{
				AddFilledSquare(field, i, rowNumber);
			}
		}
	}
}
